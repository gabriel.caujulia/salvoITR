package com.codeoftheweb.salvo;

import javax.persistence.Entity;


public enum GameStates {

    PLACESHIPS,
    WAITINGFOROPP,
    WON,
    TIE,
    LOST,
    WAIT,
    PLAY;


}
