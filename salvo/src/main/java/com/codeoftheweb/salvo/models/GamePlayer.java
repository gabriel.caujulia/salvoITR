package com.codeoftheweb.salvo.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;


@Entity
public class GamePlayer {


    @OneToMany(mappedBy = "gamePlayer", fetch = FetchType.EAGER)
    Set<Ship> ship = new HashSet<>();
    @OneToMany(mappedBy = "gamePlayer", fetch = FetchType.EAGER)
    Set<Salvo> listaSalvo = new HashSet<>();
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    //RELATIONSHIPS//
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "player_id")
    private Player player;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "game_id")
    private Game game;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "score_id")
    private Score scores;

    public GamePlayer() {
    }


    public GamePlayer(Game game, Player player) {
        this.game = game;
        this.player = player;



    }

    //GETTERS & SETTERS//
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Set<Salvo> getListaSalvo() {
        return listaSalvo;
    }

    public Set<Ship> getShip() {
        return ship;
    }

    public void setScores(Score scores) {
        this.scores = scores;
    }


    //METHODS//

    public List<String> getHitsUndefinedTurns( int turno) {
        List<String> lista = new ArrayList<>();
        int i = 1;
        while (i <= turno) {
            lista.addAll(hitsLocation(this, i));
            i++;
        }
        return lista;
    }

    public List<Map<String, Object>> DTOrepeater() {
        List<Map<String, Object>> DTOlist = new ArrayList<>();
        int x = 1;

            while (x <= getOpponent().getListaSalvo().size()) {
                DTOlist.add(this.hitPerTurnDTO( x));
                x++;
            }
        return DTOlist;
    }

    public Map<String, Object> hitPerTurnDTO( int turn) {
        Map<String, Object> dto = new LinkedHashMap<>();
        List<String> hits = this.hitsLocation(this, turn);
        dto.put("turn", turn);
        dto.put("hitLocations", hits);
        dto.put("damages", this.shipDamages(this, turn));
        dto.put("missed", 5 - this.hitsLocation(this, turn).size());
        return dto;
    }

    public Map<String, Object> gamePlayerDTO() {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", this.getId());
        dto.put("player", this.getPlayer().makeListDTO());
        return dto;
    }

    public GamePlayer getOpponent() {

        GamePlayer gpOpponent = this.getGame().getGamePlayer()
                .stream()
                .filter(gp -> gp.getId() != this.getId())
                .findFirst()
                .orElse(null);
        return gpOpponent;
    }

    public Map<String, Object> hitsDTO() {
        Map<String, Object> dto = new LinkedHashMap<>();

        if (getOpponent() == null){
            dto.put("self", new ArrayList<>());
            dto.put("opponent", new ArrayList<>());
        }else{
        dto.put("self", DTOrepeater());
        dto.put("opponent", getOpponent().DTOrepeater());}
        return dto;
    }

    public List<String> getShipLocations(GamePlayer gamePlayer) {
        return this.getShip()
                .stream()
                .map(salvo -> salvo.getLocations())
                .flatMap(location -> location.stream())
                .collect(Collectors.toList());
    }

    public List<String> hitsLocation(GamePlayer gamePlayer, int turno) {
        return
                getShipLocations(gamePlayer).stream()
                        .filter(s -> getSalvoLocationsOpponent(turno, gamePlayer)
                        .stream()
                        .anyMatch(so -> so.equals(s)))
                        .collect(Collectors.toList());
    }

    public Map<String, Object> shipDamages(GamePlayer gamePlayer, int turno) {
        Map<String, Object> damages = new LinkedHashMap<>();
        List<String> hits = this.hitsLocation(gamePlayer, turno);
        List<String> hitsUndefinedTurns = this.getHitsUndefinedTurns( turno);
        gamePlayer.getShip().forEach(ship -> damages.put(ship.getType() + "Hits", ship.getHitsInShip(hits)));
        gamePlayer.getShip().forEach(ship -> damages.put(ship.getType(), ship.getHitsInShip(hitsUndefinedTurns)));
        return damages;


    }


    public List<String> getSalvoLocationsOpponent(int turno, GamePlayer gamePlayer) {
        Optional<Salvo> salvo = getOpponent().getListaSalvo()
                .stream()
                .filter(s -> s.getTurn() == turno)
                .findFirst();
        if (salvo.isPresent())
            return salvo.get().getSalvoLocations();
        return new ArrayList<>();
    }


    public long getTotalHundidos(GamePlayer gamePlayer, List<Map<String, Object>> hits) {
        long locationsGP = this.getShip().stream().map(ship1 -> ship1.getLocations()).flatMap(ship2 -> ship2.stream()).count();
        Map<String, Object> dtoDmg = (Map<String, Object>) hits.get(hits.size()-1).get("damages");

        long totalDmg =(long)( (int) dtoDmg.get("carrier")
                + (int) dtoDmg.get("submarine")
                + (int) dtoDmg.get("destroyer")
                + (int) dtoDmg.get("patrolboat")
                + (int) dtoDmg.get("battleship"));
        System.out.println(locationsGP - totalDmg);
        return locationsGP - totalDmg;

    }

}







