package com.codeoftheweb.salvo.controller;

import com.codeoftheweb.salvo.GameStates;
import com.codeoftheweb.salvo.models.*;
import com.codeoftheweb.salvo.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api")
public class SalvoController {


    //RELATIONS//

    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private ShipRepository shipRepository;
    @Autowired
    private GamePlayerRepository gamePlayerRepository;
    @Autowired
    private SalvoRepository salvoRepository;
    @Autowired
    private ScoresRepository scoresRepository;


    public SalvoController() {
    }

    //ENDPOINTS//
    @RequestMapping("game_view/{nn}")
    private ResponseEntity<Map<String, Object>> getGameViewByGamePlayerId(@PathVariable Long nn, Authentication authentication) {


        if (isGuest(authentication)) {
            return new ResponseEntity<>(makeMap("error", "missing data"), HttpStatus.UNAUTHORIZED);
        }

        GamePlayer gamePlayer = gamePlayerRepository.findById(nn).get();
        Player player = playerRepository.findByUserName(authentication.getName());

        if (player == null) {
            return new ResponseEntity<>(makeMap("error", "something happen"), HttpStatus.UNAUTHORIZED);
        }
        if (gamePlayer == null) {
            return new ResponseEntity<>(makeMap("error", "something happen"), HttpStatus.UNAUTHORIZED);
        }
        if (gamePlayer.getPlayer().getId() != player.getId()) {
            return new ResponseEntity<>(makeMap("error", "something happen"), HttpStatus.FORBIDDEN);
        }

        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", gamePlayer.getGame().getId());
        dto.put("created", gamePlayer.getGame().getCreationDate());
        dto.put("gameState", getGameState(gamePlayer));
        dto.put("gamePlayers", gamePlayer.getGame().getGamePlayer()
                .stream()
                .map(gamePlayer2 -> gamePlayer2.gamePlayerDTO())
                .collect(Collectors.toList()));
        dto.put("ships", gamePlayer.getShip().stream().map(type -> type.makeDTO())
                .collect(Collectors.toList()));
        dto.put("salvoes", gamePlayer.getGame().getGamePlayer().stream()
                .map(gamePlayer1 -> gamePlayer1.getListaSalvo())
                .flatMap(salvos -> salvos.stream()
                        .map(salvo -> salvo.salvosDTO())));
        dto.put("hits", gamePlayer.hitsDTO());

        addScore(gamePlayer, getGameState(gamePlayer));

        return new ResponseEntity<>(dto, HttpStatus.OK);

    }


    @RequestMapping("/gamesList")
    public List<Long> getId() {
        return gameRepository
                .findAll()
                .stream()
                .map(game -> game.getId())
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/games/players/{gamePlayerId}/ships", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> placeShips(@PathVariable Long gamePlayerId, @RequestBody List<Ship> ships, Authentication authentication) {
        GamePlayer gamePlayer = gamePlayerRepository.findById(gamePlayerId).get();
        Player player = playerRepository.findByUserName(authentication.getName());


        if (gamePlayer == null) {
            return new ResponseEntity<>(makeMap("error", "you aren't login"), HttpStatus.UNAUTHORIZED);
        }
        if (player == null) {
            return new ResponseEntity<>(makeMap("error", "Payer's null"), HttpStatus.UNAUTHORIZED);
        }
        if (gamePlayer.getId() != gamePlayerId) {
            return new ResponseEntity<>(makeMap("error", "that's not your id"), HttpStatus.UNAUTHORIZED);
        }
        if (gamePlayer.getShip().size() > 0) {
            return new ResponseEntity<>(makeMap("error", "ship are already placed"), HttpStatus.FORBIDDEN);
        }
        ships.forEach(ship -> ship.setGamePlayer(gamePlayer));
        ships.forEach(ship -> shipRepository.save(ship));
        return new ResponseEntity<>(makeMap("OK", "BIEN CRACK COLOCASTE LOS BARQUITOS"), HttpStatus.CREATED);

    }

    @RequestMapping(value = "/games/players/{gamePlayerId}/salvoes", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> shotingSalvos(@PathVariable Long gamePlayerId, @RequestBody Salvo salvo, Authentication authentication) {
        GamePlayer gamePlayer = gamePlayerRepository.findById(gamePlayerId).get();
        Player player = playerRepository.findByUserName(authentication.getName());

        if (gamePlayer == null) {
            return new ResponseEntity<>(makeMap("error", "you aren't login"), HttpStatus.UNAUTHORIZED);
        }
        if (player == null) {
            return new ResponseEntity<>(makeMap("error", "username empty"), HttpStatus.UNAUTHORIZED);
        }
        if (gamePlayer.getId() != gamePlayerId) {
            return new ResponseEntity<>(makeMap("error", "wrong Id"), HttpStatus.UNAUTHORIZED);

        }

        if (gamePlayer.getListaSalvo().isEmpty()) {
            salvo.setTurn(1);
        }

        GamePlayer opponent = gamePlayer.getOpponent();

        if (opponent != null && gamePlayer.getListaSalvo().size() <= opponent.getListaSalvo().size()) {
            salvo.setTurn(gamePlayer.getListaSalvo().size() + 1);
            salvo.setGamePlayer(gamePlayer);
            salvoRepository.save(salvo);
            return new ResponseEntity<>(makeMap("OK", "salvos guardados"), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(makeMap("error", "you have already shot"), HttpStatus.FORBIDDEN);


        }


    }

    //METHODS//
    private Map<String, Object> makeMap(String key, Object value) {
        Map<String, Object> map = new HashMap<>();
        map.put(key, value);
        return map;
    }

    private boolean isGuest(Authentication authentication) {
        return authentication == null || authentication instanceof AnonymousAuthenticationToken;
    }

    public String getGameState(GamePlayer gamePlayer) {

        if (gamePlayer.getShip().isEmpty()) {
            return GameStates.PLACESHIPS.toString();
        } else if (gamePlayer.getOpponent() == null) {
            return GameStates.WAITINGFOROPP.toString();
        } else if (gamePlayer.getOpponent().getShip().size() == 0) {
            return GameStates.WAITINGFOROPP.toString();
        } else if (gamePlayer.getListaSalvo().size() == 0
                && gamePlayer.getOpponent().getListaSalvo().size() == 0) {
            return GameStates.PLAY.toString();
        } else if (gamePlayer.getListaSalvo().size() > gamePlayer.getOpponent().getListaSalvo().size()) {
            return GameStates.WAIT.toString();
        } else if (gamePlayer.getListaSalvo().size() < gamePlayer.getOpponent().getListaSalvo().size()) {
            return GameStates.PLAY.toString();
        } else if (gamePlayer.getTotalHundidos(gamePlayer, (List<Map<String, Object>>) gamePlayer.hitsDTO().get("self")) == 0
                && gamePlayer.getTotalHundidos(gamePlayer.getOpponent(), (List<Map<String, Object>>) gamePlayer.hitsDTO().get("opponent")) > 0) {
            return GameStates.LOST.toString();
        } else if (gamePlayer.getTotalHundidos(gamePlayer, (List<Map<String, Object>>) gamePlayer.hitsDTO().get("self")) > 0
                && gamePlayer.getTotalHundidos(gamePlayer.getOpponent(), (List<Map<String, Object>>) gamePlayer.hitsDTO().get("opponent")) == 0) {
            return GameStates.WON.toString();
        } else if (gamePlayer.getTotalHundidos(gamePlayer, (List<Map<String, Object>>) gamePlayer.hitsDTO().get("self")) == 0
                && gamePlayer.getTotalHundidos(gamePlayer.getOpponent(), (List<Map<String, Object>>) gamePlayer.hitsDTO().get("opponent")) == 0) {
            return GameStates.TIE.toString();
        }
        return GameStates.PLAY.toString();
    }

    private void addScore(GamePlayer gamePlayer, String ScoreGameState) {
        if (scoresRepository.findAll().stream().filter(score1 -> score1.getGame().getId() == gamePlayer.getGame().getId()).count() < 1) {
            if (ScoreGameState == "WON") {
                Score score1 = new Score(gamePlayer.getGame(), gamePlayer.getPlayer(), 1.0, LocalDateTime.now());
                Score score2 = new Score(gamePlayer.getGame(), gamePlayer.getOpponent().getPlayer(), 0.0, LocalDateTime.now());
                scoresRepository.save(score1);
                scoresRepository.save(score2);
            }
            if (ScoreGameState == "LOST") {
                Score score1 = new Score(gamePlayer.getGame(), gamePlayer.getPlayer(), 0.0, LocalDateTime.now());
                Score score2 = new Score(gamePlayer.getGame(), gamePlayer.getOpponent().getPlayer(), 1.0, LocalDateTime.now());
                scoresRepository.save(score1);
                scoresRepository.save(score2);
            }
            if (ScoreGameState == "TIE") {
                Score score1 = new Score(gamePlayer.getGame(), gamePlayer.getPlayer(), 0.5, LocalDateTime.now());
                Score score2 = new Score(gamePlayer.getGame(), gamePlayer.getOpponent().getPlayer(), 0.5, LocalDateTime.now());
                scoresRepository.save(score1);
                scoresRepository.save(score2);
            }

        }
    }

}












